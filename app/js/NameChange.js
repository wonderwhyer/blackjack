angular.module('BlackJack')
    .controller('NameController', ['$http',
        function ($http) {
            this.name = 'test';
            this.changeName = function () {
                $http.post('/changeName',
                    {
                        name: this.name
                    },
                    {
                        responseType: 'json'
                    }
                );
            }
        }
    ]);

