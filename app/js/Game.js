angular.module('BlackJack', [])
    .factory('GameControllerService', ['$http', 'GameModels', '$timeout', '$q',
        function ($http, GameModels, $timeout, $q) {
            var gameController = new GameModels.GameController();
            var delayedApply = function (actions) {
                gameController.applyAction(actions.shift());
                if (actions.length > 0) {
                    return $timeout(100, true).then(function () {
                        return delayedApply(actions);
                    });
                } else {
                    return $q.resolve(actions);
                }
            };

            var applyActions = function (response) {
                if (response.data.length > 0) {
                    return delayedApply(response.data).then(function () {
                        return response;
                    });
                }
            };

            var getLastActionTime = function () {
                var time = 0;
                if (gameController.actions && gameController.actions.length != 0) {
                    time = gameController.actions[gameController.actions.length - 1].time;
                }
                return time;
            };

            return {
                gameController: gameController,

                getNewActions: function () {
                    return $http.get('/getActions', {
                        params: {
                            time: getLastActionTime()
                        }
                    }).then(applyActions);
                },

                makeABet: function (amount) {
                    return $http.post('/makeABet',
                        {
                            amount: amount
                        }
                    );
                },
                hit: function () {
                    return $http.post('/hit', {
                        time: getLastActionTime()
                    }).then(applyActions);
                },
                stop: function () {
                    return $http.post('/stop', {
                        time: getLastActionTime()
                    }).then(applyActions);
                }
            };
        }
    ])

    .controller('Game', ['$interval', 'GameControllerService',
        function ($interval, GameControllerService) {
            this.model = GameControllerService.gameController.game;
            GameControllerService.getNewActions();
            $interval(function () {
                GameControllerService.getNewActions();
            }, 3000);
        }
    ])

    .controller('Dealer', ['GameControllerService', function (GameControllerService) {
        this.model = GameControllerService.gameController.game.dealer;
    }])

    .controller('Player', ['$scope', 'GameControllerService', function ($scope, GameControllerService) {
        this.bet = 1;

        this.makeABet = function () {
            return GameControllerService
                .makeABet(this.bet)
                .then((function (response) {
                    this.data.balance = response.data;
                    return GameControllerService.getNewActions()
                }).bind(this));
        };

        this.hit = function () {
            GameControllerService.hit().then((function () {
                this.data.balance = this.get().data.balance;
            }).bind(this));
        };

        this.stop = function () {
            GameControllerService.stop().then((function () {

            }).bind(this));
        };

        this.data = $scope.playerData;
        this.get = function () {
            var result = GameControllerService.gameController.game.findPlayer(this.data.id);
            if (result) {
                this.data.balance = result.data.balance;
            }
            return result;
        }

        this.opponents = function () {
            var id = this.data.id;
            var result = GameControllerService.gameController.game.players.filter(function (player) {
                return player.data.id != id;
            });
            return result;
        }
    }])
