function shuffle(array) {
    var currentIndex = array.length, temporaryValue, randomIndex;

    while (0 !== currentIndex) {
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;

        temporaryValue = array[currentIndex];
        array[currentIndex] = array[randomIndex];
        array[randomIndex] = temporaryValue;
    }

    return array;
}

var init = function () {
    var exports = {};

    function Card(value, kind) {
        this.value = value;
        this.kind = kind;

        this.getValue = (function () {
            return Card.VALUES[this.value];
        }).bind(this);

        this.getCompareValue = (function () {
            return Card.COMPARE_VALUES[this.value];
        }).bind(this);
    }

    Card.KIND = {
        CLUBS: 'clubs',
        DIAMONDS: 'diamonds',
        SPADES: 'spades',
        HEARTS: 'hearts'
    };

    Card.VALUES = {
        '2': 2,
        '3': 3,
        '4': 4,
        '5': 5,
        '6': 6,
        '7': 7,
        '8': 8,
        '9': 9,
        '10': 10,
        'j': 10,
        'q': 10,
        'k': 10,
        'a': 1
    };

    Card.COMPARE_VALUES = {
        '2': 0,
        '3': 0,
        '4': 0,
        '5': 0,
        '6': 0,
        '7': 0,
        '8': 0,
        '9': 0,
        '10': 0,
        'j': 1,
        'q': 2,
        'k': 3,
        'a': 4
    }

    function Cards() {
        this.cards = [];

        this.addCard = (function (card) {
            this.cards.push(card);
        }).bind(this);

        this.removeCard = (function (card) {
            this.cards.splice(this.cards.indexOf(card), 1);
        }).bind(this);

        this.shuffle = (function () {
            this.cards = shuffle(this.cards);
        }).bind(this);

        this.getValue = (function () {
            var sum = 0;
            var aces = 0;
            for (var i = 0; i < this.cards.length; i++) {
                var card = this.cards[i];
                sum += card.getValue();
                if (card.value == 'a') {
                    aces++;
                }
            }

            while (sum + 10 < 21 && aces > 0) {
                sum += 10;
                aces--;
            }
            return sum;
        }).bind(this);

        this.getCompareValue = (function () {
            var sum = 0;
            for (var i = 0; i < this.cards.length; i++) {
                var card = this.cards[i];
                sum += card.getValue();
            }
            return sum;
        }).bind(this);

        this.wins = (function (cards) {
            var thisValue = this.getValue();
            var cardsValue = cards.getValue();

            var thisBusted = thisValue > 21;
            var cardsBusted = cardsValue > 21;

            if (thisBusted && cardsBusted) {
                return Cards.RESULT.DRAW;
            } else if (thisBusted) {
                return Cards.RESULT.LOST;
            } else if (cardsBusted) {
                return Cards.RESULT.WON;
            } else {
                if (thisValue > cardsValue) {
                    return Cards.RESULT.WON;
                } else if (cardsValue > thisValue) {
                    return Cards.RESULT.LOST;
                } else {
                    if (this.cards.length < cards.cards.length) {
                        return Cards.RESULT.WON;
                    } else if (this.cards.length < cards.cards.length) {
                        return Cards.RESULT.LOST
                    } else {
                        thisValue = this.getCompareValue();
                        cardsValue = cards.getCompareValue();
                        if (thisValue > cardsValue) {
                            return Cards.RESULT.WON;
                        } else if (cardsValue > thisValue) {
                            return Cards.RESULT.LOST;
                        } else {
                            return Cards.RESULT.DRAW;
                        }
                    }
                }
            }

        }).bind(this);

        this.clear = (function () {
            this.cards = [];
        }).bind(this);

    }

    Cards.RESULT = {
        LOST: -1,
        DRAW: 0,
        WON: 1
    };

    function Player(playerData, bet) {
        this.hand = new Cards();
        this.data = playerData;
        this.bet = bet;
    }

    function Game() {
        this.dealer = new Player();
        this.players = [];

        this.findPlayer = (function (id) {
            return this.players.filter(function (player) {
                return player.data.id == id;
            })[0];
        }).bind(this);
    }

    function createDeck() {
        var deck = new Cards();
        for (var kindName in Card.KIND) {
            for (var value in Card.VALUES) {
                deck.addCard(new Card(value, Card.KIND[kindName]));
            }
        }
        deck.shuffle();
        return deck;
    }

    function GameController() {

        this.game = new Game();
        this.actions = [];

        this.applyAction = (function (action) {
            actions[action.name].apply.apply(this, action.args);
            this.actions.push(action);
        }).bind(this);


        //TODO move server helpers to different class so that they are not exposed to client
        this.dealerDrawCard = function (deck) {
            var card = deck.cards.pop();
            this.applyAction(actions.dealerDraw.create(card));
        };

        this.playerDrawCard = function (playerId, card) {
            this.applyAction(actions.playerDraw.create(playerId, card));
        };

        this.playerDone = function (player, deck) {
            this.applyAction(actions.playerDone.create(player.data.id));
            if (this.gameIsDone()) {
                this.finishTheGame(deck);
            }
        };

        this.gameIsDone = function () {
            for (var i = 0; i < this.game.players.length; i++) {
                var player = this.game.players[i];
                if (!player.done) {
                    return false;
                }
            }
            return true;
        };

        this.finishTheGame = function (deck) {
            while (this.game.dealer.hand.getValue() < 17) {
                this.dealerDrawCard(deck);
            }

            for (var i = 0; i < this.game.players.length; i++) {
                var player = this.game.players[i];
                var result = player.hand.wins(this.game.dealer.hand);
                if (result == Cards.RESULT.WON) {
                    this.applyAction(actions.playerWon.create(player));
                } else if (result == Cards.RESULT.LOST) {
                    this.applyAction(actions.playerLost.create(player));
                } else {
                    this.applyAction(actions.draw.create(player));
                }
            }
        };

        this.startNewGame = function () {
            this.applyAction(actions.startNewGame.create());
        }
    }

    function Action(name, args) {
        this.name = name;
        this.args = args;
        this.time = new Date().getTime();
    }

    var actions = {
        playerLost: {
            create: function (player) {
                return new Action('playerLost', [player.data.id, player.bet]);
            },
            apply: function (id, bet) {
                var player = this.game.findPlayer(id);
                player.bet = 0;
                player.result = 'lost';
            }
        },
        playerWon: {
            create: function (player) {
                return new Action('playerWon', [player.data.id, player.bet]);
            },
            apply: function (id, bet) {
                var player = this.game.findPlayer(id);
                player.data.balance += 2 * bet;
                player.bet = 0;
                player.result = 'won';
            }
        },
        draw: {
            create: function (player) {
                return new Action('draw', [player.data.id, player.bet]);
            },
            apply: function (id, bet) {
                var player = this.game.findPlayer(id);
                player.data.balance += bet;
                player.bet = 0;
                player.result = 'draw';
            }
        },
        dealerDraw: {
            create: function (cardInfo) {
                return new Action('dealerDraw', [cardInfo]);
            },
            apply: function (cardInfo) {
                var card = new Card(cardInfo.value, cardInfo.kind);
                this.game.dealer.hand.addCard(card);
            }
        },
        playerDraw: {
            create: function (id, cardInfo) {
                return new Action('playerDraw', [id, cardInfo]);
            },
            apply: function (id, cardInfo) {
                var player = this.game.findPlayer(id);
                var card = new Card(cardInfo.value, cardInfo.kind);
                player.hand.addCard(card);
            }
        },
        addPlayer: {
            create: function (playerData, amount) {
                return new Action('addPlayer', [playerData, amount]);
            },
            apply: function (playerData, amount) {
                var player = this.game.findPlayer(playerData.id);
                if (!player) {
                    this.game.players.push(new Player(playerData, amount));
                }
            }
        },
        playerDone: {
            create: function (playerId) {
                return new Action('playerDone', [playerId]);
            },
            apply: function (playerId) {
                var player = this.game.findPlayer(playerId);
                if (player) {
                    player.done = true;
                }
            }
        },
        startNewGame: {
            create: function () {
                return new Action('startNewGame');
            },
            apply: function () {
                this.game.players = [];
                this.game.dealer.hand.clear();
                this.actions = [];
            }
        }
    };

    exports.Card = Card;
    exports.Cards = Cards;
    exports.Player = Player;
    exports.Game = Game;
    exports.Action = Game;
    exports.actions = actions;
    exports.GameController = GameController;
    exports.createDeck = createDeck;


    return exports;
};
if (typeof module !== 'undefined' && module.exports) {
    module.exports = init();
} else {
    var models = init();
    angular.module('BlackJack')
        .factory('GameModels', function () {
            return models;
        })
}

