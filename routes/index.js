var express = require('express');
var models = require('../app/js/GameModels');
var router = express.Router();

var players = {};

var START_BALANCE = 200;

function createPlayerData(id, name) {
    if (!players[id]) {
        players[id] = {
            id: id,
            name: name,
            balance: START_BALANCE
        };
    }
    return players[id];
}

function auth(req, res, next) {
    if (req.cookies && req.cookies.username && req.cookies.id) {
        req.playerData = createPlayerData(req.cookies.id, req.cookies.username);
        next();
    } else {
        req.cookies = req.cookies || {};
        var username = req.cookies.username = "Anonymous";
        var id = req.cookies.id = Math.random() * Number.MAX_VALUE;
        res.cookies = res.cookies || {};
        res.cookie('username', "Anonymous");
        res.cookie('id', Math.random() * Number.MAX_VALUE);
        req.playerData = createPlayerData(req.cookies.id, req.cookies.username);
        next();
    }
}

var gameController = new models.GameController();
var deck;
function startNewGame() {
    gameController.startNewGame();
    deck = models.createDeck();
    gameController.dealerDrawCard(deck);
}
startNewGame();

function prepareForNewGame() {
    if (gameController.gameIsDone()) {
        setTimeout(function () {
            startNewGame();
        }, 5000);
    }
}

function getActionsSince(time) {
    return gameController.actions.filter(function (action) {
        return Number(action.time) > Number(time);
    });
}

router.get('/', auth, function (req, res) {
    var data = {
        title: 'BlackJack',
        playerData: req.playerData
    };
    res.render('index', data);
});

router.post('/changeName', auth, function (req, res) {
    var name = req.body.name;
    res.cookie('username', name);
    req.playerData.name = name;
    res.json('ok');
});

router.get('/getActions', auth, function (req, res) {
    res.json(getActionsSince(req.query.time));
});

router.post('/stop', auth, function (req, res) {
    var player = gameController.game.findPlayer(req.playerData.id);
    if (player) {
        gameController.playerDone(player, deck);
        prepareForNewGame();
    }
    res.json(getActionsSince(req.body.time));
});

router.post('/hit', auth, function (req, res) {
    var player = gameController.game.findPlayer(req.playerData.id);
    if (player) {
        gameController.playerDrawCard(req.playerData.id, deck.cards.pop());
        if (player.hand.getValue() >= 21) {
            gameController.playerDone(player, deck);
            prepareForNewGame();
        }
    }
    res.json(getActionsSince(req.body.time));
});

router.post('/makeABet', auth, function (req, res) {
    var amount = Number(req.body.amount);
    if (req.playerData.balance > amount) {
        req.playerData.balance -= amount;
        gameController.applyAction(models.actions.addPlayer.create(req.playerData, amount));
        for (var i = 0; i < 2; i++) {
            gameController.playerDrawCard(req.playerData.id, deck.cards.pop());
        }
    }
    res.json(req.playerData.balance);
});

module.exports = router;