function clear(elem) {
    return elem.getAttribute('value').then(function (text) {
        var len = text.length;
        var backspaceSeries = new Array(len + 1).join(protractor.Key.BACK_SPACE);
        elem.sendKeys(backspaceSeries);
    })
}


describe('BlackJack', function () {

    beforeEach(function () {
        browser.get('/');
    });

    describe('NameChange', function () {
        it('should allow changing name', function () {
            var nameInput = element(by.css('.user .name'));
            expect(nameInput.isPresent()).toBe(true, 'name input present');
            clear(nameInput);
            nameInput.sendKeys('TestUser');
            element(by.css('.user .btn')).click();
            browser.get('/');
            expect(element(by.css('.user .name')).getAttribute('value')).toBe('TestUser', 'name changed');
        });
    });
    /*
     describe('', function () {
     beforeEach(function () {
     });
     afterEach(function () {
     });

     it('', function () {
     });
     });
     */
});
